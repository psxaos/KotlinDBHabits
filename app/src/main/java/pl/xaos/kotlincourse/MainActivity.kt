package pl.xaos.kotlincourse

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_main.*
import pl.xaos.kotlincourse.db.HabitDbTable

class MainActivity : AppCompatActivity() {
    //we could avoid nulls using lateinit but with kotlin-extensions in gradle we can call xml properties directly
    //private lateinit var tvDescription: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//java way:
//        tvDescription = findViewById(R.id.tv_description)
//        tvDescription?.setText("A refreshing glass of water gets you hydrated")

//Kotlin way:
        recycler_view.setHasFixedSize(true)
        recycler_view.layoutManager = LinearLayoutManager(this)
        recycler_view.adapter = HabitsAdapter(HabitDbTable(this).readAllHabits())

    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (item.itemId == R.id.add_habit) {
            switchTo(CreateHabitActivity::class.java)
        }
        return true
    }

    fun switchTo(c: Class<*>) { //* is a wildcard that reference any class
        val intent = Intent(this, c)
        startActivity(intent)
    }
}
