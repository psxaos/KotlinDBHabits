package pl.xaos.kotlincourse.db

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import pl.xaos.kotlincourse.Habit
import pl.xaos.kotlincourse.db.HabitEntry.DESCR_COL
import pl.xaos.kotlincourse.db.HabitEntry.IMAGE_COL
import pl.xaos.kotlincourse.db.HabitEntry.TITLE_COL
import pl.xaos.kotlincourse.db.HabitEntry._ID
import pl.xaos.kotlincourse.db.HabitEntry.TABLE_NAME
import java.io.ByteArrayOutputStream

class HabitDbTable(context: Context) {

    private val TAG = HabitDbTable::class.java.simpleName

    private val dbHelper = HabitTrainerDb(context)

    fun store(habit: Habit): Long { //returns id of new stored object
        val db = dbHelper.writableDatabase
        val values = ContentValues()
        with(values) {
            //use "with" instead of calling all those as values.put(HabitEntry.TITLE_COL, habit.title)...
            put(TITLE_COL, habit.title)
            put(DESCR_COL, habit.description)
            put(IMAGE_COL, toByteArray(habit.image))
        }

//        db.beginTransaction()
//        val id = try {
//            val returnValue = db.insert(HabitEntry.TABLE_NAME, null, values)
//            db.setTransactionSuccessful()
//
//            returnValue //return last line as value of id
//        } finally {
//            db.endTransaction()
//        }
//        db.close()

        //use of extension function
        val id = db.transaction {
            insert(TABLE_NAME, null, values)
        }

        Log.d(TAG, "Stored new habit to the DB $habit")

        return id
    }

    fun readAllHabits(): List<Habit> {

        val columns = arrayOf(_ID, TITLE_COL, DESCR_COL, IMAGE_COL)

        val order = "${_ID} ASC"

        val db = dbHelper.readableDatabase

        val cursor = db.doQuery(TABLE_NAME, columns, orderBy = order)

        return parseHabitsFrom(cursor)
    }

    private fun parseHabitsFrom(cursor: Cursor): MutableList<Habit> {
        val habits = mutableListOf<Habit>()

        while (cursor.moveToNext()) {
            val title = cursor.getString(TITLE_COL)
            val description = cursor.getString(DESCR_COL)
            val bitmap = cursor.getBitmap(IMAGE_COL)
            habits.add(Habit(title, description, bitmap))
        }
        cursor.close()

        return habits
    }

    private fun toByteArray(bitmap: Bitmap): ByteArray {
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream)
        return stream.toByteArray()
    }
}

//extension function to remove boilerplate code commented above
private inline fun <T> SQLiteDatabase.transaction(function: SQLiteDatabase.() -> T): T { //inline prevents of creating anonymous object

    beginTransaction()
    val result = try {
        val returnValue = function() //extension function in extension function(! :/)
        setTransactionSuccessful()

        returnValue //last line is a return statement
    } finally {
        endTransaction()
    }
    close()

    return result
}

//extension function to query() method in SQLiteDatabase with default values
private fun SQLiteDatabase.doQuery(table: String,
                                   columns: Array<String>,
                                   selection: String? = null,
                                   selectionArgs: Array<String>? = null,
                                   groupBy: String? = null,
                                   having: String? = null,
                                   orderBy: String? = null): Cursor {
    return query(table, columns, selection, selectionArgs, groupBy, having, orderBy)
}

//extension function to simplyfy getting String from the lolumn by passing not int as in Cusros API but passing column name
private fun Cursor.getString(columnName: String) = getString(getColumnIndex(columnName)) //oneline function = instead of {return getString(...)}

//extension function to get bitmmap from DB by calling a column name and converting Blob int bitmap
private fun Cursor.getBitmap(columnName: String): Bitmap {
    val bytes = getBlob(getColumnIndex(columnName))
    return BitmapFactory.decodeByteArray(bytes, 0, bytes.size)
}